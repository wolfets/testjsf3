package testJSF3;

import javax.enterprise.inject.Model;
import javax.faces.bean.ManagedBean;
import javax.ws.rs.core.Response;

@ManagedBean
public class Login {
	public Login() {
		super();
		setName("toto");
		setRespValid("");
	}

	private String name;
	private String pwd;

	private String buttonHeader;
	private String respValid;

	public String getName() {
		return name;
	}

	public String getRespValid() {
		return respValid;
	}

	public void setRespValid(String resp) {
		this.respValid = resp;
	}

	public String getButtonHeader() {
		return "Validez";
	}

	public void setButtonHeader(String buttonHeader) {
		this.buttonHeader = buttonHeader;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String valid() {
		if (getName().equalsIgnoreCase("toto")) {
			setRespValid("ok");
			return "OK";
		} else {
			setRespValid("pas bon");
			return "";
		}


	}
}
